var args = arguments[0] || {};
var state = 'otp';
var nsAskAuth = {};
var slideIn = Titanium.UI.createAnimation({
	bottom : 0
});
var slideOut = Titanium.UI.createAnimation({
	bottom : '-251dp'
}, function() {
	$.win.fireEvent('focus');
});
// var countryCode = Ti.Locale.getCurrentCountry();

$.win.addEventListener('androidback', function() {
	Titanium.App.fireEvent('closeApp');
});

$.submit.addEventListener('click', function() {
	// TODO: validation

	var mobile_number = $.country_code.value + $.mobile.value;
	mobile_number = Alloy.Globals.UTILS.removeCharacters(mobile_number);

	if (state == 'otp') {
		// Get otp service call

		Alloy.Globals.API.registerMobile({
			mobile_number : mobile_number
		}, function(response) {
			console.error('success ', response);
			// On success
			state = 'login';
			$.submit.title = 'Submit';
			$.otp.visible = true;
			$.mobile.touchEnabled = false;
		}, function(error) {
			console.error('error ', error);
		});

	} else {

		Alloy.Globals.API.validateOtp({
			mobile_number : mobile_number,
			otp : $.otp.value
		}, function(response) {

			console.error('success ', response);
			Titanium.App.Properties.setString('my_number', mobile_number);
			Titanium.App.Properties.setString('auth_token', response.token);

			console.log('my_number', 'token', Titanium.App.Properties.getString('my_number'), Titanium.App.Properties.getString('auth_token'));

			Alloy.Globals.UTILS.contactPermissions(function() {
				$.win.close();
				if (!OS_IOS) {
					var people = Ti.Contacts.getAllPeople();
					Titanium.App.Properties.setObject('people', people);
				}
				$.args.callback();
			});
		}, function(error) {
			console.error('error ', error);
		});

	}
});

// Alloy.Globals.UTILS.contactPermissions(function(){
// console.error('JHGQFJHDSGJD');
// });

$.win.addEventListener('focus', function() {

	if (OS_IOS) {
		// $.country.value = Utils.getCountryName(countryCode);
		// country_picker_view.setSelectedRow(0, Utils.countryIndex(countryCode), false);

		// $.country_code.value = phone_codes[0];
		// country_picker_view.setSelectedRow(0, phone_codes[0], false);
	}

	// if (OS_IOS) {
	// // $.country.value = Utils.getCountryName(countryCode);
	// country_picker_view.setSelectedRow(0, Utils.countryIndex(countryCode), false);
	//
	// console.error('stateCode ', stateCode);
	// if (countryCode == 'US') {
	// $.state.value = Utils.getStateName(stateCode);
	// state_picker_view.setSelectedRow(0, Utils.stateIndex(stateCode), false);
	// }
	// }
	// if (OS_ANDROID) {
	// setTimeout(function() {
	// $.country.setSelectedRow(0, Utils.countryIndex(countryCode), false);
	// $.country_label.text = $.country.getSelectedRow(0);
	//
	// if (countryCode == 'US') {
	// $.state.setSelectedRow(0, Utils.stateIndex(stateCode), false);
	// console.log('$.state.getSelectedRow(0) ', $.state.getSelectedRow(0));
	// stateCode = Utils.getStateCode($.state.getSelectedRow(0).title);
	// Ti.API.info('stateCode 5 ', stateCode);
	// $.state_label.text = $.state.getSelectedRow(0).title;
	// $.state_view.height = Titanium.UI.SIZE;
	// } else {
	// $.state_view.height = 0;
	// }
	// }, 1200);
	// }
});

$.country_code.addEventListener('click', function() {
	country_picker_container.animate(slideIn);
	//$.country_code.value = phone_codes[0];
	// country_picker_view.setSelectedRow(0, phone_codes[0], false);
});

var phone_codes = ['+1', '+44', '+91', '+966', '+971'];
var country_picker = [];
var country_picker_container = '',
    country_picker_view = '',
    picker_value = '';
for (var i = 0,
    j = phone_codes.length; i < j; i++) {
	var code = phone_codes[i];
	country_picker[i] = Ti.UI.createPickerRow({
		title : code
	});
}
nsAskAuth.init = function() {
	if (OS_IOS) {
		country_picker_container = Titanium.UI.createView({
			height : '251dp',
			bottom : '-251dp',
			backgroundColor : '#fff'
		});

		var done = Titanium.UI.createButton({
			top : 0,
			right : '10dp',
			style : 0,
			font : {
				fontSize : '14dp',
			},
			color : '#4a79ff',
			height : '35dp',
			title : L('done_btn'),
		});
		done.addEventListener('click', function() {
			country_picker_container.animate(slideOut);
		});
		country_picker_container.add(done);

		country_picker_view = Ti.UI.createPicker({
			bottom : 0
		});

		country_picker_view.add(country_picker);
		country_picker_view.selectionIndicator = true;

		country_picker_view.addEventListener('change', function(e) {
			Ti.API.info('countryCode ', JSON.stringify(e));
			picker_value = phone_codes[e.rowIndex];
			Ti.API.info('picker_value ', picker_value);
			$.country_code.value = picker_value;
			// country_picker_view.setSelectedRow(0, phone_codes[0], false);

		});

		country_picker_container.add(country_picker_view);
		$.win.add(country_picker_container);

		$.country_code.value = phone_codes[0];
		// country_picker_view.setSelectedRow(0, 0, false);
		// $.country_label.text = $.country_picker_view.getSelectedRow(0);
		// country_picker_view.setSelectedRow(0, phone_codes[0], false);

		console.error('phone_codes[0] ', phone_codes[0]);
	} else {
		setTimeout(function() {
			$.country_code.add(country_picker);
		}, 1000);

		$.country_code.addEventListener('click', function() {
			$.mobile.blur();
		});

		$.country_code.addEventListener('change', function(e) {
			// Ti.API.info('countryCode ', JSON.stringify(e));
			picker_value = phone_codes[e.rowIndex];
			// Ti.API.info('picker_value ', picker_value);
			$.country_code.setSelectedRow(0, phone_codes.indexOf(picker_value), false);
			$.country_code_label.text = picker_value;
			$.country_code.value = picker_value;
		});

		$.country_code.setSelectedRow(0, phone_codes.indexOf(phone_codes[0]), false);
		$.country_code_label.text = phone_codes[0];
		$.country_code.value = phone_codes[0];
	}

};

nsAskAuth.init();

