var add_group_name = {};

Titanium.App.addEventListener('showGroupButton', function() {
	$.group_icon.visible = true;
});

Titanium.App.addEventListener('hideGroupButton', function() {
	$.group_icon.visible = false;
});

Titanium.App.addEventListener('showIndividualButton', function() {
	$.individual_icon.visible = true;
});

Titanium.App.addEventListener('hideIndividualButton', function() {
	$.individual_icon.visible = false;
});

$.group_icon.addEventListener('click', function() {
	var current_page = Alloy.Globals.current_page;
	var no_of_contacts_selected = Alloy.Globals.no_of_contacts_selected;
	console.log('Alloy.Globals.current_page, Alloy.Globals.no_of_contacts_selected ', Alloy.Globals.current_page, Alloy.Globals.no_of_contacts_selected);
	if (current_page == 'contacts' && Alloy.Globals.no_of_contacts_selected > 1) {
		// Add a group name
		Alloy.createController('wrapper/ask_groupname_input').getView().open();
	}
});

$.individual_icon.addEventListener('click', function() {
	var single_ele_array = [];
	for (var i in Alloy.Globals.contacts_selected) {
		single_ele_array.push(Alloy.Globals.UTILS.updateNumber(Alloy.Globals.contacts_selected[i]));
		console.log('single_ele_array ', single_ele_array);
		Alloy.Globals.API.sendTagNotification({
			numbers_array : single_ele_array
		}, function(response){
			console.log('response ', response);
			Alloy.Globals.toast('Sent tag request!!!');
		}, function(error){
			console.log('error ', error);
            Alloy.Globals.toast('Tag Req: some thing goes wrong!!!');
		});
		single_ele_array = [];
	}
});

$.user_picture.addEventListener('click', function() {
	Alloy.Globals.openWindow('user/profile', {}, L('profile'), true);
});
