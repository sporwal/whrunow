var nsProfile = {};
var name_state = 'edit';

$.camera_view.addEventListener('click', function() {
	Alloy.Globals.UTILS.uploadImage(function(image) {
		$.user_picture.image = image;
	});
});

$.logout.addEventListener('click', function() {
	// TODO: Clear the data
	// Back to login screen
	// Clear contacts from first screen
	Titanium.App.Properties.removeAllProperties();
	// Titanium.App.Properties.setString('auth_token', response.token);
	
});

$.edit.addEventListener('click', function() {
	if (name_state == 'edit') {
		$.name.editable = true;
		name_state = 'save';
		$.edit.text = '\uf0c7';
		$.name.focus();
	} else {
		$.name.editable = false;
		name_state = 'edit';
		$.edit.text = '\uf040';
		$.name.blur();
	}

});

nsProfile.init = function() {
	$.name.value = 'Shraddha';
	$.mobile_number.text = '+91467862732';
};

nsProfile.init();
