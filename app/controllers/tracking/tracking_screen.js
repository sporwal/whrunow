var nsTrackingScreen = {};
var mapModule = require('ti.map');
nsTrackingScreen.init = function() {
	Alloy.Globals.UTILS.geolocationPermissions(function(e) {
		Alloy.Globals.UTILS.getLocation(function(coords) {
			console.error('COORDINATES coords ', coords);
			$.map_view.region = {
				latitude : coords.latitude,
				longitude : coords.longitude,
				latitudeDelta : 0.05,
				longitudeDelta : 0.05
			};

			var annotations = [mapModule.createAnnotation({
				latitude : coords.latitude,
				longitude : coords.longitude,

				// title : data.coordinates.destination.title,
				// subtitle : data.coordinates.destination.subtitle + " - " + data.coordinates.destination.type,
				// image : data.marker,
				pincolor : mapModule.ANNOTATION_PURPLE,
				animate : true,
				// pincolor : mapModule.ANNOTATION_GREEN,
			}), mapModule.createAnnotation({
				latitude : 12.922550,
				longitude : 77.595149,
				// title : data.coordinates.origin.title,
				// subtitle : data.coordinates.origin.subtitle,
				pincolor : mapModule.ANNOTATION_RED,
				// image : '/images/driver_marker.png',
				animate : true
			})];

			$.map_view.annotations = annotations;
		});
	});

};

nsTrackingScreen.init();
