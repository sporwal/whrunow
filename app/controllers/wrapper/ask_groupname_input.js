$.cancel.addEventListener('click', function() {
	$.container.close();
});

$.submit.addEventListener('click', function() {
	// call create group service: TODO
	// service call to add a group or views
	var array_to_be_sent = [];

	for (var i in Alloy.Globals.contacts_selected) {
		array_to_be_sent.push(Alloy.Globals.UTILS.updateNumber(Alloy.Globals.contacts_selected[i]));
	}

	console.log('array_to_be_sent ', array_to_be_sent);
	Alloy.Globals.API.sendTagNotification({
		numbers_array : array_to_be_sent,
		group_name : $.group_name.value
	}, function(response) {
		// refresh group or views
		console.log('group response ', response);
		Alloy.Globals.toast('Sent group tag request!!!');

		// After service success
		Alloy.Globals.contacts_selected = [];
		$.container.close();
	}, function(error) {
		console.log('group error ', error);
		Alloy.Globals.toast('Group Tag Req: some thing goes wrong!!!');
		Alloy.Globals.contacts_selected = [];
		$.container.close();
	});

});
