// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var nsAllContacts = {};
var current_listview = '';

function toggleTag(e) {

	var item = e.section.getItemAt(e.itemIndex);
	if (item.properties.tagged) {
		item.properties.tagged = false;
		item.tag_switch.backgroundImage = '/ui/off_switch.png';
	} else {
		item.properties.tagged = true;
		item.tag_switch.backgroundImage = '/ui/on_switch.png';
	}
	e.section.updateItemAt(e.itemIndex, item);
};

function accept_request(e){
	console.error('accept_request CALLED');
	// TODO: Call accept service
	
	// On success
	// remove element from requestList App properties
	// Remove element from the listview 
	// For list view operations, refer: https://gist.github.com/mstepanov/5201041
}

function decline_request(e){
	console.error('decline_request CALLED');
	// TODO: Call decline service
	
	// On success
	// remove element from requestList App properties
	// Remove element from the listview 
	// For list view operations, refer: https://gist.github.com/mstepanov/5201041
}

// console.error('contact_details -----> ', JSON.stringify($.args.contacts));

nsAllContacts.load_active_views = function() {
	current_listview = 'active';
	$.all_contacts.defaultItemTemplate = 'views_template';
	var views_data = [];
	var active = $.args.active;
	for (var i in active) {

		views_data.push({
			chat_image : {
				image : (active[i].type == 'user' || active[i].type == 'request') ? '/ui/user_placeholder.png' : '/ui/group_placeholder.png'
			},
			name : {
				text : active[i].name,
				color : '#000'
			},
			mobile : {
				text : active[i].mobile,
				color : '#000'
			},
			tag_switch : {
				visible : (active[i].type == 'user') ? true : false,
				backgroundImage : (active[i].tagged) ? '/ui/on_switch.png' : '/ui/off_switch.png'
			},
			request_views : {
				visible : (active[i].type == 'request') ? true : false,
			},
			properties : {
				accessoryType : Ti.UI.LIST_ACCESSORY_TYPE_NONE,
				color : '#000',
				searchableText : active[i].name,
				backgroundColor : 'transparent',
				tagged : active[i].tagged
			}
		});
	}
	$.all_contacts.sections[0].setItems(views_data);
};

nsAllContacts.load_contacts = function() {
	current_listview = 'all_contacts';
	$.all_contacts.defaultItemTemplate = 'contacts_template';
	var contacts_data = [];
	var contacts = $.args.contacts;
	for (var i in contacts) {
		contacts_data.push({
			name : {
				text : contacts[i].name,
				color : '#000'
			},
			mobile : {
				text : contacts[i].mobile,
				color : '#000'
			},
			properties : {
				accessoryType : Ti.UI.LIST_ACCESSORY_TYPE_NONE,
				color : '#000',
				searchableText : contacts[i].name,
				backgroundColor : 'transparent',
				number: contacts[i].mobile
			}
		});
	}
	$.all_contacts.sections[0].setItems(contacts_data);
	// $.all_contacts.searchView = $.searchBar;
};

nsAllContacts.init = function() {

	nsAllContacts.load_contacts();
};

nsAllContacts.init();

Titanium.App.addEventListener('changeView', function(e) {

	console.error(JSON.stringify(e));
	Alloy.Globals.contacts_selected = [];
	if (current_listview == 'all_contacts') {
		nsAllContacts.load_active_views();
	} else {
		nsAllContacts.load_contacts();
	}
});

var selected = 0;
// var 
$.all_contacts.addEventListener('itemclick', function(e) {

	var item = e.section.getItemAt(e.itemIndex);
	if (current_listview == 'all_contacts') {
		var item = e.section.getItemAt(e.itemIndex);
		if (item.properties.accessoryType == Ti.UI.LIST_ACCESSORY_TYPE_NONE) {
			item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK;
			selected++;
			
			// Add number to array
			Alloy.Globals.contacts_selected.push(item.properties.number);
			console.log('Alloy.Globals.contacts_selected ', Alloy.Globals.contacts_selected);
			
		} else {
			item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_NONE;
			selected--;
			
			// Remove number from array
			var number_index = Alloy.Globals.contacts_selected.indexOf(item.properties.number);
			console.log('number_index ', number_index);
			Alloy.Globals.contacts_selected.splice(number_index, 1);
			
			console.log('Alloy.Globals.contacts_selected ', Alloy.Globals.contacts_selected);
		}
		e.section.updateItemAt(e.itemIndex, item);
		if (selected > 0) {
			if (selected > 1) {
				Titanium.App.fireEvent('showGroupButton');
			} else {
				Titanium.App.fireEvent('showIndividualButton');
				Titanium.App.fireEvent('hideGroupButton');
			}
		} else {
			Titanium.App.fireEvent('hideGroupButton');
			Titanium.App.fireEvent('hideIndividualButton');
		}

		Alloy.Globals.no_of_contacts_selected = selected;
	} else {

		console.error('e.source ', JSON.stringify(e.source));

		// TODO: open map window
	}
});

console.debug('end of contact_details');
