var contact_wrapper = {};
var contacts = [];
var current_view = '';
var view_properties_white = {
	backgroundColor : '#fff'
};
var view_properties_theme = {
	backgroundColor : Alloy.Globals.theme.theme_color
};
var lbl_properties_white = {
	color : '#fff',
};
var lbl_properties_theme = {
	color : Alloy.Globals.theme.theme_color
};

// Alloy.Globals.current_view_obj = $.container;

var views = [{
	name : 'Name1',
	mobile : '3323232',
	id : 'qwqewq',
	tagged : true,
	type : 'user'
}, {
	name : 'Name2',
	mobile : '978364839',
	id : 'seasas',
	tagged : false,
	type : 'user'
}, {
	name : 'Group1',
	mobile : '32323223',
	id : 'jkaehsk',
	tagged : true,
	type : 'group'
}, {
	name : 'Name4',
	mobile : '3323232',
	id : 'qwqewq',
	tagged : true,
	type : 'user'
}, {
	name : 'Group2',
	mobile : '3323232',
	id : 'qwqewq',
	tagged : true,
	type : 'group'
}, {
	name : 'Name5',
	mobile : '3323232',
	id : 'qwqewq',
	tagged : false,
	type : 'user'
}];

if (Titanium.App.Properties.getObject('requestList')) {
	var requestList = Titanium.App.Properties.getObject('requestList');
	for (var i in requestList) {
		views.push(requestList[i]);
	}
}

contact_wrapper.populate_views = function() {
	Titanium.App.fireEvent('hideGroupButton');
	Titanium.App.fireEvent('hideIndividualButton');

	// Titanium.App.fireEvent('changeView');
	current_view = 'views';
	// $.wrapper_container.removeAllChildren();
	$.views_view.applyProperties(view_properties_white);
	$.views_image.applyProperties(lbl_properties_theme);
	$.views_label.applyProperties(lbl_properties_theme);

	$.contacts_view.applyProperties(view_properties_theme);
	$.contacts_image.applyProperties(lbl_properties_white);
	$.contacts_label.applyProperties(lbl_properties_white);
	return;

	/*
	 $.wrapper_container.removeAllChildren();

	 $.views_view.applyProperties(view_properties_white);
	 $.views_image.applyProperties(lbl_properties_theme);
	 $.views_label.applyProperties(lbl_properties_theme);

	 $.contacts_view.applyProperties(view_properties_theme);
	 $.contacts_image.applyProperties(lbl_properties_white);
	 $.contacts_label.applyProperties(lbl_properties_white);

	 current_view = 'views';
	 Alloy.Globals.current_page = current_view;

	 var views = [{
	 name : 'Name1',
	 mobile : '3323232',
	 id : 'qwqewq'
	 }, {
	 name : 'Name2',
	 mobile : '978364839',
	 id : 'seasas'
	 }, {
	 name : 'Name3',
	 mobile : '32323223',
	 id : 'jkaehsk'
	 }];
	 var open_views = Alloy.createController('wrapper/active_views_view', views).getView();
	 $.wrapper_container.add(open_views);*/

};

var populate_contacts = function() {

	Titanium.App.fireEvent('hideGroupButton');
	Titanium.App.fireEvent('hideIndividualButton');

	// Titanium.App.fireEvent('changeView');
	$.contacts_view.applyProperties(view_properties_white);
	$.contacts_image.applyProperties(lbl_properties_theme);
	$.contacts_label.applyProperties(lbl_properties_theme);

	$.views_view.applyProperties(view_properties_theme);
	$.views_image.applyProperties(lbl_properties_white);
	$.views_label.applyProperties(lbl_properties_white);

	// $.wrapper_container.removeAllChildren();

	console.error('COMING HERE....2!!!');
	var people = {};
	if (OS_IOS) {
		people = Ti.Contacts.getAllPeople();
	} else {
		people = Titanium.App.Properties.getObject('people', {});
	}
	if (!contacts.length) {
		for (i in people) {
			for (var j in people[i].phone.mobile) {
				contacts.push({
					name : people[i].fullName,
					mobile : people[i].phone.mobile[j]
				});
			}
		}
	}

	Ti.API.info('Total contacts: ' + contacts.length);
	console.error('contacts ', JSON.stringify(contacts));
	var data = {
		contacts : contacts,
		active : views
	};
	var all_contacts = Alloy.createController('wrapper/all_contacts_view', data).getView();
	$.wrapper_container.add(all_contacts);
	Alloy.Globals.loading.hide();
};

contact_wrapper.open_contact_list = function() {

	console.error('COMING HERE....1!!! ', Ti.Contacts.hasContactsPermissions());

	Alloy.Globals.UTILS.contactPermissions(function() {
		//Titanium.App.fireEvent('changeView');
		current_view = 'contacts';
		Alloy.Globals.current_page = current_view;
		populate_contacts();
		console.error('COMING HERE....>>>> ', Ti.Contacts.hasContactsPermissions());
	});
};

$.views_view.addEventListener('click', function() {
	console.log('current_view ', current_view);
	if (current_view != 'views') {
		contact_wrapper.populate_views();
		Titanium.App.fireEvent('changeView');
	};
});

$.contacts_view.addEventListener('click', function() {
	console.log('current_view ', current_view);
	if (current_view != 'contacts') {
		contact_wrapper.open_contact_list();
		Titanium.App.fireEvent('changeView');
	};
});

contact_wrapper.postCheck = function() {
	console.log('POST CHECK');
	if (!(Titanium.App.Properties.getString('my_number') && Titanium.App.Properties.getString('auth_token'))) {
		Alloy.createController('auth/ask_authentication', {
			callback : function() {
				console.log('CALLBACK CALLED');
				contact_wrapper.open_contact_list();
			}
		}).getView().open();
	} else {
		contact_wrapper.open_contact_list();
	}

	$.container.removeEventListener('postlayout', contact_wrapper.postCheck);
};

contact_wrapper.init = function() {
	Alloy.Globals.loading.show();
	console.log('CONTACT WRAPPER INIT ');
	// contact_wrapper.open_contact_list();
	$.container.addEventListener('postlayout', contact_wrapper.postCheck);
};
contact_wrapper.init();
//Alloy.createController('auth/ask_authentication').getView().open();
console.log('END OF CONTACT WRAPPER');

