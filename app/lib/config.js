/*
 config.js
 DTDD
 
 Created by Shraddha on 2015-05-08.
 Copyright 2015 DTDD. All rights reserved.

*/

var config = {};

config.baseURL = Alloy.CFG.baseURL;

config.baseDriverURL = Alloy.CFG.baseDriverURL;

config.socketURL = Alloy.CFG.socketURL;

config.allDrivers = Alloy.CFG.allDrivers;

module.exports = config;
