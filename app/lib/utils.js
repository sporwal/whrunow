/**
 * Get country code
 */
var phone_codes = ['+1', '+44', '+91', '+966', '+971'];
var codes = [{
	country : 'IN',
	code : '0091'
}];

exports.getCountryCode = function() {

	return phone_codes;
};

exports.removeCharacters = function(string) {
	var regex = /\D+/g;
	console.log('string.replace(regex) ', string.replace(regex, ''));
	string = '00' + string.replace(regex, '');
	return string;
};

exports.updateNumber = function(string) {
	var regex = /\D+/g;
	string = string.replace(regex, '');
	if (string.length == 11) {
		string = string.substring(1, string.length);
		console.log('string -- updated number ', string);
		string = '0091' + string;
	}else if(string.length == 10){
        string = '0091' + string;
    } else {
		string = '00' + string;
	}
	return string;

};

// Upload picture
exports.uploadImage = function(callback) {

	// http://sapandiwakar.in/resizing-images-using-appcelerator-titanium/
	var dialog = Ti.UI.createOptionDialog({
		options : ['Camera', 'Gallery', 'Cancel'],
		title : 'Upload image using?'
	});

	dialog.show();

	dialog.addEventListener('click', function(e) {

		if (e.index === 0) {

			//Open Camera
			// Alloy.Globals.loading.show();
			Ti.Media.showCamera({
				saveToPhotoGallery : true,

				success : function(event) {

					var img = event.media;
					// img = img.imageAsResized(240, 160);
					callback(img);
				},

				cancel : function(e) {
					// Alloy.Globals.loading.hide();
				},

				error : function(e) {
					// Alloy.Globals.loading.hide();
				},

				showControls : true,
				mediaTypes : Ti.Media.MEDIA_TYPE_PHOTO,
				autohide : true
			});
		} else if (e.index === 1) {

			//Open gallery
			// Alloy.Globals.loading.show();
			Ti.Media.openPhotoGallery({
				success : function(event) {

					var img = event.media;
					// img = img.imageAsResized(240, 160);
					callback(img);
				},

				cancel : function(e) {
					// Alloy.Globals.loading.hide();
				},

				error : function(e) {
					// Alloy.Globals.loading.hide();
				},
			});
		} else {
			// Do nothing
		}
		dialog.hide();
	});
};

/**
 * Fired when user taps on the Ti.Geolocation button.
 */
exports.geolocationPermissions = function geolocation(callback) {

	// Let's include some related properties for iOS we already had
	if (OS_IOS) {

		// Available since Ti 5.0
		console.error('Ti.Geolocation.allowsBackgroundLocationUpdates', Ti.Geolocation.allowsBackgroundLocationUpdates);

		// Available since Ti 0.x,
		// Always returns true on Android>2.2
		console.error('Ti.Geolocation.locationServicesEnabled', Ti.Geolocation.locationServicesEnabled);
	}

	// The new cross-platform way to check permissions
	// The first argument is required on iOS and ignored on other platforms
	var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);
	console.error('Ti.Geolocation.hasLocationPermissions', hasLocationPermissions);

	if (hasLocationPermissions) {
		// return alert('You already have permission.');
		callback(true);
	}

	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {

		// Map constants to names
		var map = {};
		map[Ti.Geolocation.AUTHORIZATION_ALWAYS] = 'AUTHORIZATION_ALWAYS';
		map[Ti.Geolocation.AUTHORIZATION_AUTHORIZED] = 'AUTHORIZATION_AUTHORIZED';
		map[Ti.Geolocation.AUTHORIZATION_DENIED] = 'AUTHORIZATION_DENIED';
		map[Ti.Geolocation.AUTHORIZATION_RESTRICTED] = 'AUTHORIZATION_RESTRICTED';
		map[Ti.Geolocation.AUTHORIZATION_UNKNOWN] = 'AUTHORIZATION_UNKNOWN';
		map[Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE] = 'AUTHORIZATION_WHEN_IN_USE';

		// Available since Ti 0.8 for iOS and Ti 4.1 for Windows
		// Always returns AUTHORIZATION_UNKNOWN on iOS<4.2
		var locationServicesAuthorization = Ti.Geolocation.locationServicesAuthorization;
		console.error('Ti.Geolocation.locationServicesAuthorization', 'Ti.Geolocation.' + map[locationServicesAuthorization]);

		if (locationServicesAuthorization === Ti.Geolocation.AUTHORIZATION_RESTRICTED) {
			return alert('Because permission are restricted by some policy which you as user cannot change, we don\'t request as that might also cause issues.');

		} else if (locationServicesAuthorization === Ti.Calendar.AUTHORIZATION_DENIED) {
			return dialogs.confirm({
				title : 'You denied permission before',
				message : 'We don\'t request again as that won\'t show the dialog anyway. Instead, press Yes to open the Settings App to grant permission there.',
				// callback: editPermissions
			});
		}
	}

	// The new cross-platform way to request permissions
	// The first argument is required on iOS and ignored on other platforms
	Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
		console.error('Ti.Geolocation.requestLocationPermissions', e);

		if (e.success) {

			// Instead, probably call the same method you call if hasLocationPermissions() is true
			// alert('You granted permission.');
			callback(true);

		} else if (OS_ANDROID) {
			alert('You denied permission for now, forever or the dialog did not show at all because it you denied forever before.');

		} else {

			// We already check AUTHORIZATION_DENIED earlier so we can be sure it was denied now and not before
			Ti.UI.createAlertDialog({
				title : 'You denied permission.',

				// We also end up here if the NSLocationAlwaysUsageDescription is missing from tiapp.xml in which case e.error will say so
				message : e.error
			}).show();
		}
	});
};

// exports.geolocation = geolocation;
exports.getLocation = function getLocation(callback) {
	console.error('GET LOCATION CALLED ');
	Titanium.Geolocation.distanceFilter = 10;
	//Check if Geolocation is enabled
	//locationEnableCheck(function() {

	if (OS_ANDROID) {

		Titanium.Geolocation.purpose = "Find location of device.";
		Titanium.Geolocation.manualMode = true;
		Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;

		var gpsProvider = Titanium.Geolocation.Android.createLocationProvider({
			name : Titanium.Geolocation.PROVIDER_NETWORK || Titanium.Geolocation.PROVIDER_GPS,
			minUpdateTime : 60,
			minUpdateDistance : 100
		});
		Titanium.Geolocation.Android.addLocationProvider(gpsProvider);

		var gpsRule = Titanium.Geolocation.Android.createLocationRule({
			provider : Titanium.Geolocation.PROVIDER_GPS || Titanium.Geolocation.PROVIDER_NETWORK,
			// Updates should be accurate to 100m
			accuracy : 100,
			// Updates should be no older than 5m
			maxAge : 300000,
			// But  no more frequent than once per 10 seconds
			minAge : 10000
		});
		Titanium.Geolocation.Android.addLocationRule(gpsRule);

	} else {

		Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;

		Titanium.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
		var authCode = Titanium.Geolocation.locationServicesAuthorization;

		console.debug("authCode ", authCode);

		if (authCode === 0 || authCode === "AUTHORIZATION_DENIED" || authCode === "AUTHORIZATION_RESTRICTED") {

			// Not authorized
			console.debug('NOT AUTHORIZED :(');
			var confirm = Titanium.UI.createAlertDialog({
				title : L("appName"),
				message : L("userDisabledLocation_err"),
				buttonNames : ['OK']
			});

			confirm.show();

			confirm.addEventListener('click', function(e) {
				console.debug(JSON.stringify(e));
				if (e.index === 0) {
					confirm.hide();
				}
			});
		} else {
			console.debug(' AUTHORIZED, authCode = ', authCode);
			auth = true;
		}
	}
	Titanium.Geolocation.getCurrentPosition(function(e) {
		if (OS_ANDROID) {
			Alloy.CFG.isSimulator = false;
		};
		if (Alloy.CFG.isSimulator) {
			e = {
				coords : {
					"latitude" : 12.9218715,
					"longitude" : 77.5841288
				}
			};
			callback(e.coords);
		} else if (e.error) {
			Ti.API.info("Alloy.Globals.getCurrentLocation(): Error in getting current position. - ", e.error);
			var confirm = Titanium.UI.createAlertDialog({
				title : L("appName"),
				message : L("locationData_err"),
				buttonNames : ['OK']
			});

			confirm.show();

			confirm.addEventListener('click', function(e) {
				console.debug(JSON.stringify(e));
				if (e.index === 0) {
					confirm.hide();
					Titanium.App.fireEvent('closeApp');
				}
			});
		} else {
			Ti.API.info("Alloy.Globals.getCurrentLocation(): Success in reading current position - [" + e.coords.latitude + ", " + e.coords.longitude + "].");
			console.log('GPS RESULTS ', JSON.stringify(e));

			if (e.coords.latitude && e.coords.longitude) {
				callback(e.coords);
			} else {

				var confirm = Titanium.UI.createAlertDialog({
					title : L("appName"),
					message : L("locationData_err"),
					buttonNames : ['OK']
				});

				confirm.show();

				confirm.addEventListener('click', function(e) {
					console.debug(JSON.stringify(e));
					if (e.index === 0) {
						getLocation(callback);
					}
				});

			}
		}
	});
	// });
};

exports.contactPermissions = function(callback) {

	var hasContactsPermissions = Ti.Contacts.hasContactsPermissions();
	console.error('Ti.Contacts.hasContactsPermissions', hasContactsPermissions);

	if (hasContactsPermissions) {

		// We have to actually use a Ti.Contacts method for the permissions to be generated
		// FIXME: https://jira.appcelerator.org/browse/TIMOB-19933
		if (OS_IOS) {
			console.error('Ti.Contacts.getAllGroups', Ti.Contacts.getAllGroups());
		}

		// return alert('You already have permission.');
		// populate_contacts();
		if (callback) {
			console.log('You already have permission.');
			callback(true);
		};
	}

	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {

		// Map constants to names
		var map = {};
		map[Ti.Contacts.AUTHORIZATION_AUTHORIZED] = 'AUTHORIZATION_AUTHORIZED';
		map[Ti.Contacts.AUTHORIZATION_DENIED] = 'AUTHORIZATION_DENIED';
		map[Ti.Contacts.AUTHORIZATION_RESTRICTED] = 'AUTHORIZATION_RESTRICTED';
		map[Ti.Contacts.AUTHORIZATION_UNKNOWN] = 'AUTHORIZATION_UNKNOWN';

		// Available since Ti 2.1.3 and always returns AUTHORIZATION_AUTHORIZED on iOS<6 and Android
		var contactsAuthorization = Ti.Contacts.contactsAuthorization;
		console.error('Ti.Contacts.contactsAuthorization', 'Ti.Contacts.' + map[contactsAuthorization]);

		if (contactsAuthorization === Ti.Contacts.AUTHORIZATION_RESTRICTED) {
			return alert('Because permission are restricted by some policy which you as user cannot change, we don\'t request as that might also cause issues.');

		} else if (contactsAuthorization === Ti.Calendar.AUTHORIZATION_DENIED) {
			return dialogs.confirm({
				title : 'You denied permission before',
				message : 'We don\'t request again as that won\'t show the dialog anyway. Instead, press Yes to open the Settings App to grant permission there.',
				// callback : editPermissions
			});
		}
	}

	// The new cross-platform way to request permissions
	if (OS_IOS || Ti.Platform.Android.API_LEVEL >= 23) {
		Ti.Contacts.requestContactsPermissions(function(e) {
			console.error('Ti.Contacts.requestContactsPermissions', e);

			if (e.success) {

				// Instead, probably call the same method you call if hasContactsPermissions() is true
				// alert('You granted permission.');
				// populate_contacts();
				if (callback) {
					callback(true);
				};

			} else if (OS_ANDROID) {
				alert('You have denied permission for now, forever or the dialog did not show at all because you denied forever before.');

			} else {

				// We already check AUTHORIZATION_DENIED earlier so we can be sure it was denied now and not before
				alert('You denied permission.');
			}
		});
	}
};
