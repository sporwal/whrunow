/***
 api.js
 rRunow.net

 Created by Shraddha on 2017-01-05.
 Copyright 2017 by Shraddha. All rights reserved.

 */

var config = require('config');
var moment = require('alloy/moment');

var api = {};

/*
 * Function for http request
 */
function httpRequest(endpoint, method, data, successFunction, errorFunction, fileType) {

	if (!Ti.Network.online) {

		Alloy.Globals.UTILS.networkCheck();

		if (errorFunction) {

			errorFunction();
		}
		return;
	}

	var url = config.baseURL + endpoint;

	if (data && method == 'GET') {

		url = url;

	}

	var xhr = Ti.Network.createHTTPClient();

	var retries = 0;

	xhr.onload = function() {

		Ti.API.info(endpoint, this.responseText);

		if (this.status == '200') {

			try {

				var responseJSON = JSON.parse(this.responseText);
				console.log('RESPONSE ', JSON.stringify(responseJSON));

				// if (responseJSON && !responseJSON.error) {
				if (responseJSON && responseJSON.success) {

					console.log('SUCCESS RESPONSE ', JSON.stringify(responseJSON));

					if (successFunction) {

						successFunction(responseJSON);
					}
					// } else if (errorFunction && responseJSON && responseJSON.error) {
				} else if (errorFunction && responseJSON && !responseJSON.success) {// TODO

					console.log('ERROR RESPONSE ', JSON.stringify(responseJSON));

					// if (responseJSON.error == "Invalid or expired token.") { // TODO
					//
					// Alloy.Globals.currentUser = null;
					// Ti.App.Properties.setString('token', null);
					// Ti.App.fireEvent('loggedIn');
					// alert('Your session has expired. Please login again');
					// }

					errorFunction(responseJSON.message);

				}
			} catch (e) {

				console.log('IN CATCH BLOCK RESPONSE ', JSON.stringify(responseJSON));

				if (errorFunction) {

					errorFunction(e);
				}
				Ti.API.error(endpoint, e);
			}
		} else {

			console.log('IN ELSE BLOCK RESPONSE ', JSON.stringify(responseJSON));

			if (errorFunction) {

				errorFunction(this.response);
			}
			Ti.API.error(this.response);
		}

		// Alloy.Globals.loading.hide();
	};

	xhr.onerror = function(e) {

		// if (retries < 3) {
		//
		// retries++;
		// doRequest();
		// } else {

		// Alloy.Globals.loading.hide();
		Ti.API.info('Transmission error: ' + endpoint + ' ' + JSON.stringify(this) + this.responseText);

		alert(L('connection_issue'));

		if (errorFunction && this.responseText) {

			errorFunction(this.responseText);

		} else if (errorFunction) {

			errorFunction(e);
		}

		// Alloy.Globals.loading.hide();
		// }
	};

	xhr.timeout = 60000;

	function doRequest() {

		// Alloy.Globals.loading.show();

		xhr.open(method, url);

		if (fileType !== "media") {
			xhr.setRequestHeader("Content-Type", "application/json");
		}
		// console.log(Titanium.App.Properties.getString('my_number') + Titanium.App.Properties.getString('auth_token'));
		// xhr.setRequestHeader("Authorization", "Basic " + (Titanium.App.Properties.getString('my_number') + Titanium.App.Properties.getString('auth_token')));

		console.log('QWERTYUIOIUYTREDCFGYU ',(Titanium.App.Properties.getString('my_number')+':' + Titanium.App.Properties.getString('auth_token')));

		if (Titanium.App.Properties.getString('my_number') && Titanium.App.Properties.getString('auth_token')) {
			console.error(Titanium.Utils.base64encode(Titanium.App.Properties.getString('my_number')+':' + Titanium.App.Properties.getString('auth_token')));
			xhr.setRequestHeader("Authorization", "Basic " + Titanium.Utils.base64encode(Titanium.App.Properties.getString('my_number') + ':' + Titanium.App.Properties.getString('auth_token')));
		}

		if (fileType === "media") {
			xhr.setRequestHeader("enctype", "multipart/form-data");
			Ti.API.info('gonna hit ' + url + ' and gonna send multipart/form-data ' + JSON.stringify(data));
			xhr.send(data);

		} else if (data && method == 'POST') {

			Ti.API.info('gonna hit ' + url + ' and gonna send ' + JSON.stringify(data));
			xhr.send(JSON.stringify(data));
		} else {

			Ti.API.info('gonna hit ' + url);
			xhr.send();
		}
	}

	doRequest();

}

// Authentication
api.registerMobile = function(args, success, fail) {

	var endPoint = 'registerUser?mobileNumber=' + args.mobile_number;

	httpRequest(endPoint, 'GET', {}, success, fail);
};

api.validateOtp = function(args, success, fail) {

	var endPoint = 'validateOtp';
	var data = {
		mobileNumber : args.mobile_number,
		otp : args.otp,
		deviceRegId : Titanium.App.Properties.getString('device_token')
	};

	httpRequest(endPoint, 'POST', data, success, fail);
};

api.isRegistered = function(args, success, fail){

	var endPoint = 'checkIfRegistered?mobileNumber=' + args.mobile_number;

	httpRequest(endPoint, 'GET', {}, success, fail);
};

api.sendTagNotification = function(args, success, fail) {

	var endPoint = 'tag/tagNumbers';
	var data = {
		userNumber : Titanium.App.Properties.getString('my_number'),
		mobileNumbers : args.numbers_array
	};
	
	if(args.group_name){
		data.nameOfGroup = args.group_name;
	}

	httpRequest(endPoint, 'POST', data, success, fail);
};

api.respondTag = function(args, success, fail) {

	var endPoint = 'tag/' + args.response;
	var data = {
		userNumber : args.my_number,
		mobileNumbers : args.numbers_array
	};

	httpRequest(endPoint, 'POST', data, success, fail);
};

api.goOnline = function(args, success, fail) {

	var endPoint = 'tag/updateStatus';
	var data = {
		tagId : args.tag_id,
		mobileNumber : Titanium.App.Properties.getString('my_number'),
		status : args.status
	};

	httpRequest(endPoint, 'POST', data, success, fail);
};

api.updateLocationDetails = function(args, success, fail) {

	var endPoint = 'tag/updateLocationDetails';
	var data = {
		tagId : args.tag_id,
		mobileNumber : Titanium.App.Properties.getString('my_number'),
		latitude : args.latitude,
		longitude : args.longitude
	};

	httpRequest(endPoint, 'POST', data, success, fail);
};

module.exports = api;
