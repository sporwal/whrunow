// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

/**
 * FOR PUSH NOTIFICATION
 * https://github.com/morinel/gcmpush
 * https://github.com/HazemKhaled/TiPushNotification
 *
 * http://www.appcelerator.com/blog/2014/09/new-in-sdk-3-4-ios-8-interactive-notifications/
 * https://wiki.appcelerator.org/display/guides2/Notification+Services
 * https://wiki.appcelerator.org/display/guides2/iOS+Interactive+Notifications#iOSInteractiveNotifications-LockScreenorNotificationCenter
 * http://docs.appcelerator.com/platform/latest/#!/guide/Subscribing_to_push_notifications
 * https://blog.serverdensity.com/how-to-build-an-apple-push-notification-provider-server-tutorial/
 * http://stackoverflow.com/questions/12519438/apns-setup-for-the-server
 * https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html#//apple_ref/doc/uid/TP40008194-CH8-SW1
 * http://quickblox.com/developers/How_to_create_APNS_certificates
 *
 */

Alloy.Globals = _.extend(Alloy.Globals, {
	theme : {
		theme_color : '#FAC131',
		color_white : '#ffffff',
		color_dark : '#000000',
		bg_color : '#ffffff',
		cancel_color : '#cc0000'

	},
	userdata : {
		auth : {
			login : false,
			mobile : ''
		},
	}
});

Alloy.Globals.contacts_selected = [];
var request = Alloy.createController('wrapper/request_tag').getView();
// TODO: On tag request - show the window
Titanium.App.addEventListener('receivedTagRequest', function() {
	// Alloy.Globals.current_view_obj.add(request);

	// Refresh views TODO

});

Titanium.App.addEventListener('completeTagRequest', function() {
	Alloy.Globals.current_view_obj.remove(request);
});

// API
Alloy.Globals.API = require('api');
Alloy.Globals.UTILS = require('/utils');

Alloy.Globals.loading = Alloy.createWidget("nl.fokkezb.loading");
var toast = Alloy.createWidget('nl.fokkezb.toast', 'global', {
	// defaults
});
Alloy.Globals.toast = toast.show;
// same as toast.info
Alloy.Globals.error = toast.error;
// applies the 'error' theme

if(!Titanium.App.Properties.getObject('requestList')){
	Titanium.App.Properties.setObject('requestList', []);
	var requestList = Titanium.App.Properties.getObject('requestList');
	
	/*
		TODO: jsut for a sample
	*/
	requestList.push({
		type: 'request',
		name: 'Test name',
		id: '3232'
	});
	Titanium.App.Properties.setObject('requestList', requestList);
	/*
		TODO: jsut for a sample
	*/
}

// TODO: the key=='pinme' will contain this:
/*
{
	var requestList = Titanium.App.Properties.getObject('requestList');
	requestList.push({
		type: 'request',
		name: 'Test name',
		id: '3232'
	});
	Titanium.App.Properties.setObject('requestList', requestList);
}
*/

Alloy.Globals.jolicode = {};
Alloy.Globals.jolicode.pageflow = {};
Alloy.Globals.jolicode.pageflow.height = Ti.Platform.displayCaps.platformHeight;
Alloy.Globals.jolicode.pageflow.width = Ti.Platform.displayCaps.platformWidth;

if (OS_ANDROID) {
	Alloy.Globals.jolicode.pageflow.height = Ti.Platform.displayCaps.platformHeight / Ti.Platform.displayCaps.logicalDensityFactor;
	Alloy.Globals.jolicode.pageflow.width = Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor;
}

// Open a controller
Alloy.Globals.openWindow = function(controller, arguments, title_text, newOne, rightMenu) {

	console.error('rightMenurightMenu ', JSON.stringify(rightMenu));

	if (Alloy.Globals.pageflow.getCurrentPage() == null || newOne === true) {

		Alloy.Globals.pageflow.addChild({
			arguments : arguments,
			controller : controller,
			backButton : {
				image : '/images/back.png',
				tintColor : '#ffffff',
				width : '35dp',
				height : '35dp',
				left : 0,
				backgroundColor : Alloy.Globals.theme.theme_color,
				hidden : newOne === true ? false : true
			},
			navBar : {
				backgroundColor : Alloy.Globals.theme.theme_color,
				// left : 'misc/openMenu',
				right : rightMenu,
				title : title_text,
				titleOptions : {
					color : '#000',
					font : {
						fontSize : '17dp',
						// fontWeight : 'bold'
					},
					width : Titanium.UI.SIZE
				}
			},
			direction : {
				top : 0,
				left : 1
			}
		});

		if (!newOne) {

			currentPage = controller;
		}
	} else if (currentPage != controller) {

		Alloy.Globals.pageflow.replacePage(0, {
			arguments : arguments,
			controller : controller,
			backButton : {
				hidden : true
			},
			navBar : {
				backgroundColor : Alloy.Globals.theme.theme_color,
				// left : 'misc/openMenu',
				right : rightMenu,
				title : title_text,
				titleOptions : {
					color : '#000',
					font : {
						fontSize : '17dp',
						// fontWeight : 'bold'
					},
					width : Titanium.UI.SIZE
				}
			},
			direction : {
				top : 0,
				left : 1
			}
		});
		currentPage = controller;
	}
};

//........

// TODO: http://www.tidev.io/2013/12/20/using-gcm-for-android-push-notifications-with-acs/

if (OS_ANDROID) {
	var CloudPush = require('ti.cloudpush');
	var deviceToken = null;
	CloudPush.retrieveDeviceToken({
		success : function deviceTokenSuccess(e) {
			Ti.API.info('Device Token: ' + e.deviceToken);
			deviceToken = e.deviceToken;
		},
		error : function deviceTokenError(e) {
			alert('Failed to register for push! ' + e.error);
		}
	});
	CloudPush.showTrayNotification = true;
	CloudPush.enablePush = true;

	CloudPush.addEventListener('callback', function(evt) {
		
		console.error('CloudPush callback CALLED ', JSON.stringify(evt));
		alert(evt.payload);
		// enablePush.title = CloudPush.enabled ? 'Push Enabled' : 'Push Disabled';
		// showAppOnTrayClick.title = CloudPush.showAppOnTrayClick ? 'Tray Click Shows App' : 'Tray Click Does Nothing';
		// showTrayNotification.title = CloudPush.showTrayNotification ? 'Show in Tray' : 'Do Not Show in Tray';
		// focusAppOnPush.title = CloudPush.focusAppOnPush ? 'Push Focuses App' : 'Push Doesn\'t Focus App';
		// showTrayNotificationsWhenFocused.title = CloudPush.showTrayNotificationsWhenFocused ? 'Show Trays when Focused' : 'Hide Trays when Focused';
		// singleCallback.title = CloudPush.singleCallback ? 'Callbacks trigger one by one' : 'Callbacks trigger together';

	});
	CloudPush.addEventListener('trayClickLaunchedApp', function(evt) {
		Ti.API.info('Tray Click Launched App (app was not running)');
	});
	CloudPush.addEventListener('trayClickFocusedApp', function(evt) {
		Ti.API.info('Tray Click Focused App (app was already running)');
	});

}

// TODO: https://wiki.appcelerator.org/display/guides2/Android+Notifications#AndroidNotifications-Customlayout
// http://kiteplans.info/2015/09/01/appcelerator-titanium-alloy-jpush-push-notification-module-android-update-local-notifications-custom-layout/
// http://kiteplans.info/2015/04/04/appcelerator-titanium-alloy-jpush-push-notification-module-ios-and-android-china-push-notification-non-gcm/

// var acceptOrDecline = Titanium.Android.createRemoteViews({
// layoutId : Titanium.App.Android.R.layout.acceptdeclinenotificationview
// });
//
// // Reference elements in the layout by prefixing the IDs with 'Ti.App.Android.R.id'
// acceptOrDecline.setTextViewText(Titanium.App.Android.R.id.message, 'Wants to pin with you');
// acceptOrDecline.setTextViewText(Titanium.App.Android.R.id.acceptbutton, 'Accept');
// acceptOrDecline.setTextViewText(Titanium.App.Android.R.id.declinebutton, 'Decline');
// acceptOrDecline.setOnClickPendingIntent(Titanium.App.Android.R.id.acceptbutton, function() {
// var data = {
// tagId : event.tagId,
// mobileNumber : event.mobileNumber,
// tagResponse : 'A'
// };
// Alloy.Globals.UTILS.respondTag(data, function(response) {
// console.error('response respondTag ', JSON.stringify(response));
// }, function(error) {
// console.error('error respondTag ', JSON.stringify(error));
// });
// });
//
// acceptOrDecline.setOnClickPendingIntent(Titanium.App.Android.R.id.declinebutton, function() {
// var data = {
// tagId : event.tagId,
// mobileNumber : event.mobileNumber,
// tagResponse : 'D'
// };
// Alloy.Globals.UTILS.respondTag(data, function(response) {
// console.error('response respondTag ', JSON.stringify(response));
// }, function(error) {
// console.error('error respondTag ', JSON.stringify(error));
// });
// });
/*

 if (Ti.Platform.name === 'android') {
 // set android-only options
 var pnOptions = {
 senderId : '552362636060', // It's the same as your project id
 notificationSettings : {
 // sound: 'mysound.mp3', // Place soudn file in platform/android/res/raw/mysound.mp3
 smallIcon : 'appicon.png', // Place icon in platform/android/res/drawable/notification_icon.png
 largeIcon : 'appicon.png', // Same
 // vibrate: true, // Whether the phone should vibrate
 insistent : true, // Whether the notification should be insistent
 // group: 'News', // Name of group to group similar notifications together
 localOnly : false, // Whether this notification should be bridged to other devices
 priority : 2, // Notification priority, from -2 to 2
 defaults : Titanium.Android.DEFAULT_ALL,
 tickerText: 'Wants to pin with you.'
 }
 };

 } else if (Ti.Platform.name === 'iPhone OS') {// set ios-only options.
 // Sets interactive notifications as well if iOS8 and above. Interactive notifications is optional.
 if (parseInt(Ti.Platform.version.split(".")[0], 10) >= 8) {
 var thumbUpAction = Ti.App.iOS.createUserNotificationAction({
 identifier : "THUMBUP_IDENTIFIER",
 title : "Agree",
 activationMode : Ti.App.iOS.USER_NOTIFICATION_ACTIVATION_MODE_BACKGROUND,
 destructive : false,
 authenticationRequired : false
 });

 var thumbDownAction = Ti.App.iOS.createUserNotificationAction({
 identifier : "THUMBDOWN_IDENTIFIER",
 title : "Disagree",
 activationMode : Ti.App.iOS.USER_NOTIFICATION_ACTIVATION_MODE_BACKGROUND,
 destructive : false,
 authenticationRequired : false
 });

 var thumbUpDownCategory = Ti.App.iOS.createUserNotificationCategory({
 identifier : "THUMBUPDOWN_CATEGORY",
 // The following actions will be displayed for an alert dialog
 actionsForDefaultContext : [thumbUpAction, thumbDownAction],
 // The following actions will be displayed for all other notifications
 actionsForMinimalContext : [thumbUpAction, thumbDownAction]
 });

 var pnOptions = {
 types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND],
 categories : [thumbUpDownCategory]
 };
 } else {//No support for interactive notifications, omit categories
 var pnOptions = {
 types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND]
 };
 }
 }

 // set cross-platform event
 var onReceive = function(event) {

 // Your code here
 console.error('NOTIFICATION EVENT ', event);
 console.error('NOTIFICATION EVENT ', JSON.stringify(event));

 if (event.key == 'pinme') {
 console.error('DFGHJAJHAGDHJGAHJGD');
 // Alloy.createController('wrapper/ask_groupname_input').getView().open();
 // var notification = Titanium.Android.createNotification({
 // contentTitle: 'sdfsdfsdfsdfsdfsdf',
 // contentText: 'WERTYUIO'
 // });
 // Titanium.Android.NotificationManager.notify(1, notification);
 }
 };

 // Create instance with base url
 var tiPush = require('ti-push-notification').init({
 backendUrl : 'http://vrunow.net/'
 });

 // register this device
 tiPush.registerDevice({
 pnOptions : pnOptions,
 onReceive : onReceive,

 });

 */
